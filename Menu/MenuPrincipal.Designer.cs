﻿namespace Menu
{
    partial class MenuPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.hacer_Suma = new System.Windows.Forms.Button();
            this.hacer_Resta = new System.Windows.Forms.Button();
            this.hacer_Mult = new System.Windows.Forms.Button();
            this.hacer_Div = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.showRes = new System.Windows.Forms.Button();
            this.variable_1 = new System.Windows.Forms.Label();
            this.variable_2 = new System.Windows.Forms.Label();
            this.res_Suma = new System.Windows.Forms.Label();
            this.res_Resta = new System.Windows.Forms.Label();
            this.res_Mult = new System.Windows.Forms.Label();
            this.res_Div = new System.Windows.Forms.Label();
            this.variable_3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // hacer_Suma
            // 
            this.hacer_Suma.Location = new System.Drawing.Point(122, 81);
            this.hacer_Suma.Name = "hacer_Suma";
            this.hacer_Suma.Size = new System.Drawing.Size(84, 23);
            this.hacer_Suma.TabIndex = 0;
            this.hacer_Suma.Text = "Suma";
            this.hacer_Suma.UseVisualStyleBackColor = true;
            this.hacer_Suma.Click += new System.EventHandler(this.button1_Click);
            // 
            // hacer_Resta
            // 
            this.hacer_Resta.Location = new System.Drawing.Point(122, 110);
            this.hacer_Resta.Name = "hacer_Resta";
            this.hacer_Resta.Size = new System.Drawing.Size(84, 23);
            this.hacer_Resta.TabIndex = 1;
            this.hacer_Resta.Text = "Resta";
            this.hacer_Resta.UseVisualStyleBackColor = true;
            this.hacer_Resta.Click += new System.EventHandler(this.button2_Click);
            // 
            // hacer_Mult
            // 
            this.hacer_Mult.Location = new System.Drawing.Point(122, 139);
            this.hacer_Mult.Name = "hacer_Mult";
            this.hacer_Mult.Size = new System.Drawing.Size(84, 23);
            this.hacer_Mult.TabIndex = 2;
            this.hacer_Mult.Text = "Multiplicación";
            this.hacer_Mult.UseVisualStyleBackColor = true;
            this.hacer_Mult.Click += new System.EventHandler(this.button3_Click);
            // 
            // hacer_Div
            // 
            this.hacer_Div.Location = new System.Drawing.Point(122, 168);
            this.hacer_Div.Name = "hacer_Div";
            this.hacer_Div.Size = new System.Drawing.Size(84, 23);
            this.hacer_Div.TabIndex = 3;
            this.hacer_Div.Text = "División";
            this.hacer_Div.UseVisualStyleBackColor = true;
            this.hacer_Div.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(41, 81);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 110);
            this.button5.TabIndex = 4;
            this.button5.Text = "Captura";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // showRes
            // 
            this.showRes.Location = new System.Drawing.Point(213, 81);
            this.showRes.Name = "showRes";
            this.showRes.Size = new System.Drawing.Size(75, 110);
            this.showRes.TabIndex = 5;
            this.showRes.Text = "Resultados";
            this.showRes.UseVisualStyleBackColor = true;
            this.showRes.Click += new System.EventHandler(this.button6_Click);
            // 
            // variable_1
            // 
            this.variable_1.AutoSize = true;
            this.variable_1.Location = new System.Drawing.Point(41, 44);
            this.variable_1.Name = "variable_1";
            this.variable_1.Size = new System.Drawing.Size(35, 13);
            this.variable_1.TabIndex = 6;
            this.variable_1.Text = "label1";
            // 
            // variable_2
            // 
            this.variable_2.AutoSize = true;
            this.variable_2.Location = new System.Drawing.Point(119, 44);
            this.variable_2.Name = "variable_2";
            this.variable_2.Size = new System.Drawing.Size(35, 13);
            this.variable_2.TabIndex = 7;
            this.variable_2.Text = "label2";
            // 
            // res_Suma
            // 
            this.res_Suma.AutoSize = true;
            this.res_Suma.Location = new System.Drawing.Point(295, 81);
            this.res_Suma.Name = "res_Suma";
            this.res_Suma.Size = new System.Drawing.Size(35, 13);
            this.res_Suma.TabIndex = 8;
            this.res_Suma.Text = "label1";
            // 
            // res_Resta
            // 
            this.res_Resta.AutoSize = true;
            this.res_Resta.Location = new System.Drawing.Point(295, 110);
            this.res_Resta.Name = "res_Resta";
            this.res_Resta.Size = new System.Drawing.Size(35, 13);
            this.res_Resta.TabIndex = 9;
            this.res_Resta.Text = "label3";
            // 
            // res_Mult
            // 
            this.res_Mult.AutoSize = true;
            this.res_Mult.Location = new System.Drawing.Point(295, 139);
            this.res_Mult.Name = "res_Mult";
            this.res_Mult.Size = new System.Drawing.Size(35, 13);
            this.res_Mult.TabIndex = 10;
            this.res_Mult.Text = "label3";
            // 
            // res_Div
            // 
            this.res_Div.AutoSize = true;
            this.res_Div.Location = new System.Drawing.Point(295, 168);
            this.res_Div.Name = "res_Div";
            this.res_Div.Size = new System.Drawing.Size(35, 13);
            this.res_Div.TabIndex = 11;
            this.res_Div.Text = "label3";
            // 
            // variable_3
            // 
            this.variable_3.AutoSize = true;
            this.variable_3.Location = new System.Drawing.Point(210, 44);
            this.variable_3.Name = "variable_3";
            this.variable_3.Size = new System.Drawing.Size(35, 13);
            this.variable_3.TabIndex = 12;
            this.variable_3.Text = "label2";
            // 
            // MenuPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(426, 296);
            this.Controls.Add(this.variable_3);
            this.Controls.Add(this.res_Div);
            this.Controls.Add(this.res_Mult);
            this.Controls.Add(this.res_Resta);
            this.Controls.Add(this.res_Suma);
            this.Controls.Add(this.variable_2);
            this.Controls.Add(this.variable_1);
            this.Controls.Add(this.showRes);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.hacer_Div);
            this.Controls.Add(this.hacer_Mult);
            this.Controls.Add(this.hacer_Resta);
            this.Controls.Add(this.hacer_Suma);
            this.Name = "MenuPrincipal";
            this.Text = "Tarea 1 - Luis Azcuaga";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button hacer_Suma;
        private System.Windows.Forms.Button hacer_Resta;
        private System.Windows.Forms.Button hacer_Mult;
        private System.Windows.Forms.Button hacer_Div;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button showRes;
        private System.Windows.Forms.Label variable_1;
        private System.Windows.Forms.Label variable_2;
        private System.Windows.Forms.Label res_Suma;
        private System.Windows.Forms.Label res_Resta;
        private System.Windows.Forms.Label res_Mult;
        private System.Windows.Forms.Label res_Div;
        private System.Windows.Forms.Label variable_3;
    }
}

