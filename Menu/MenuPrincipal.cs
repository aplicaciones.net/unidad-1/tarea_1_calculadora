﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Menu
{
    public partial class MenuPrincipal : Form
    {
        Captura.MenuCaptura menu = new Captura.MenuCaptura();
        Int16 suma, resta, mult, div;
        Boolean err_div = false;

        public MenuPrincipal()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            variable_1.Text = Convert.ToString(Captura.MenuCaptura.captura[0]);
            variable_2.Text = Convert.ToString(Captura.MenuCaptura.captura[1]);
            variable_3.Text = Convert.ToString(Captura.MenuCaptura.captura[2]);
            suma = Convert.ToInt16(Captura.MenuCaptura.captura[0] + Captura.MenuCaptura.captura[1] + Captura.MenuCaptura.captura[2]);


        }

        private void button2_Click(object sender, EventArgs e)
        {
            resta = Convert.ToInt16(Captura.MenuCaptura.captura[0] - Captura.MenuCaptura.captura[1] - Captura.MenuCaptura.captura[2]);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            mult = Convert.ToInt16(Captura.MenuCaptura.captura[0] * Captura.MenuCaptura.captura[1] * Captura.MenuCaptura.captura[2]);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (Captura.MenuCaptura.captura[0] != 0 && Captura.MenuCaptura.captura[1] != 0 && Captura.MenuCaptura.captura[2] != 0)
            {
                err_div = false;
                div = Convert.ToInt16(Captura.MenuCaptura.captura[0] / Captura.MenuCaptura.captura[1] / Captura.MenuCaptura.captura[2]);
            } else
            {
                err_div = true;
            }
            
        }

        private void button6_Click(object sender, EventArgs e)
        {
                res_Suma.Text = Convert.ToString(suma);
                res_Resta.Text = Convert.ToString(resta);
                res_Mult.Text = Convert.ToString(mult);
            if (!err_div)
            {
                res_Div.Text = Convert.ToString(div);
            }
            else
            {
                res_Div.Text = "ERROR";
            }




        }

        private void button5_Click(object sender, EventArgs e)
        {
            menu.Show();
            
        }
        
        private void Form1_Load(object sender, EventArgs e)
        {
        }
    }
}
