﻿namespace Captura
{
    partial class MenuCaptura
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.aceptar_Variables = new System.Windows.Forms.Button();
            this.var_1 = new System.Windows.Forms.TextBox();
            this.var_2 = new System.Windows.Forms.TextBox();
            this.var_3 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // aceptar_Variables
            // 
            this.aceptar_Variables.Location = new System.Drawing.Point(89, 118);
            this.aceptar_Variables.Name = "aceptar_Variables";
            this.aceptar_Variables.Size = new System.Drawing.Size(75, 23);
            this.aceptar_Variables.TabIndex = 0;
            this.aceptar_Variables.Text = "Aceptar";
            this.aceptar_Variables.UseVisualStyleBackColor = true;
            this.aceptar_Variables.Click += new System.EventHandler(this.aceptar_Variables_Click);
            // 
            // var_1
            // 
            this.var_1.Location = new System.Drawing.Point(73, 22);
            this.var_1.Name = "var_1";
            this.var_1.Size = new System.Drawing.Size(100, 20);
            this.var_1.TabIndex = 1;
            // 
            // var_2
            // 
            this.var_2.Location = new System.Drawing.Point(73, 48);
            this.var_2.Name = "var_2";
            this.var_2.Size = new System.Drawing.Size(100, 20);
            this.var_2.TabIndex = 2;
            // 
            // var_3
            // 
            this.var_3.Location = new System.Drawing.Point(73, 74);
            this.var_3.Name = "var_3";
            this.var_3.Size = new System.Drawing.Size(100, 20);
            this.var_3.TabIndex = 3;
            // 
            // MenuCaptura
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(246, 163);
            this.Controls.Add(this.var_3);
            this.Controls.Add(this.var_2);
            this.Controls.Add(this.var_1);
            this.Controls.Add(this.aceptar_Variables);
            this.Name = "MenuCaptura";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button aceptar_Variables;
        private System.Windows.Forms.TextBox var_1;
        private System.Windows.Forms.TextBox var_2;
        private System.Windows.Forms.TextBox var_3;
    }
}

