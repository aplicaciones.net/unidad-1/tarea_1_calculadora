﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Captura
{
    public partial class MenuCaptura : Form
    {
        public static Int16[] captura = new Int16[3];

        public MenuCaptura()
        {
            InitializeComponent();
        }

        private void aceptar_Variables_Click(object sender, EventArgs e)
        {
            captura[0] = Convert.ToInt16(var_1.Text);
            captura[1] = Convert.ToInt16(var_2.Text);
            captura[2] = Convert.ToInt16(var_3.Text);
            
            //send to main class
            Hide();
        }
    }
}
